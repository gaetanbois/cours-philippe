# JS, seconde section

Philippe Pary

Pour chaque exercice, pensez à utiliser git, évidemment

## Exercice 8

1. Créez un dossier JS, créez y un fichier `script.js`
2. Liez le fichier à la page de l’exercice 6
3. Ajoutez-y le morceau de code suivant :


    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }


    function setRandomColor(element) {
        element.style = "background-color:" + getRandomColor();
    }

4. Modifiez le `onClick` des div pour un `setRandomColor(this)`
5. WAW, non ?

## Théorie

### Et concrètement ?

Le JavaScript doit être incorporé via un fichier extérieur. Le code JavaScript directement écrit dans une page ou dans les balises HTML sera à proscrire dès le prochain cours !

Comme en CSS, ce qui sépare les lignes, ce sont des ; ou les accolades { }

On trouve quelques grandes notions que nous allons aborder:
* variables
* opérateurs de contrôle
* fonctions

### variables

Une variable est une petite case dans laquelle on va stocker une valeur. L’astuce est qu’on ne sait pas ce que c’est que cette valeur … 
Elle va servir à stocker une information être utilisée plus tard.

Quand on sait de manière certaine la valeur, on parle d’une constante (logique, non ?)


Comment je déclare une variable ?

1. une déclaration de varialbe directe
    maVariable = document.getElementById("myName").value;
2. une variable vide
    var maVariable; // ou let maVariable;

Pourquoi créer une variable vide ? Pour être certain qu’elle existe ! Des erreurs peuvent apparaître quand on teste une variable qui n’existe pas …

### Exercice 9

1. Créez une nouvelle page HTML avec son fichier CSS et son fichier JS
2. Créez un champ d’input
3. Dans le fichier JS, déclarez une variable vide `myName`;
4. En cas de changement de la valeur de l’input, affichez `myName` puis attribuez la valeur du champ à la variable `myName` (onChange="console.log(myName);myName=this.value")
5. Virez la déclaration de la variable dans le fichier JS, observez la différence de comportement

6. Créez un formulaire complet pour demander l’état civil du visiteur
7. avec `onclick` sur un bouton en bas du formulaire, faites afficher les informations

### Les fonctions

Une fonction est un morceau de code.

Quand on le rédige on *déclare* la fonction : le code ne se lance pas, il est juste enregistré sagement dans un coin.

Une fois la fonction *déclarée*, on peut l’*appeler* : on lance le code et il va s’éxecuter. On peut *appeler* autant de fois la fonction qu’on veut.

On a deux syntaxes pour **déclarer** une fonction:

La moderne (à privilégier)

    var myFunction = function(parameter1, parameter2) {
        return parameter1 + parameter2;
    }

À l’ancienne (proche de PHP)

    function myFunction(parameter1, parameter2) {
        return parameter1 + parameter2;
    }


On a une syntaxe pour **appeler** une fonction, la même que partout:

    console.log(myFunction(3, 2));

(En réalité, [il en existe une autre](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Function/call) qui est bien pratique dans la vraie vie, mais bon, hein. On va l’ignorer)

### Exercice 10

1. Créez un HTML/CSS/JS
2. Créez une fonction appelée *addition*
3. Créez deux inputs numériques et un bouton
4. En clic sur le bouton, affichez le résultat de l’addition dans une div

tip: document.getElementById("myDiv").innerHTML = addition(&lt;oh ?&gt;, &lt;et ici ?&gt;);

5. Mais … mais le résultat est chelou, non ?
6. Ce qui est issu des champs de formulaire est traité comme du texte ! Et on sait que `+` avec le texte, ça ajoute à la suite …
7. Donc dans la fonction addition doit faire `return parseInt(parameter1) + parseInt(parameter2)`

### Les structures de contrôle

Les structures de contrôles sont très utiles dans la vie. Elles permettent:

* d’éxecuter, ou non, du code
* d’éxecuter une fois ou plusieurs fois un même morceau de code

Elles sont la base de la programmation, elles sont communes à tous les langages

#### if

On teste une condition. Si elle est vraie, on éxecute un code.

    if(myVar == 1) {
        console.log("La variable vaut 1");
    }

Pour comparer on a des opérateurs de comparaison:

* == : la valeur est égale
* < : la valeur est inférieure
* > : la valeur est supérieure
* <= : la valeur est inférieure ou égale
* >= : la valeur est supérieure ou égale

On abordera la logique plus tard

#### else

Après un `if`, si la condition est fausse, on éxecute un autre code.

    if(myVar == 1) {
        console.log("La variable vaut 1");
    }
    else {
        console.log("La variable ne vaut pas 1");
    }

Notez, un nouveau if peut suivre un else. On voit ça plus tard

### Exercice 11

1. Créez un projet HTML/CSS/JS
2. Ajoutez un input de type numérique
3. En cas de changement de valeur, écrivez dans une div si la valeur est paire ou impaire

tip: if (myVar%2 == 0)

tip2: n’oubliez par le `parseInt()` !

### for

La boucle `for` répète une action un certain nombre de fois.

    for (var i=0; i < 10; i++) {
        console.log("La variable i vaut " + i)
    }

Cette boucle s’éxecute 10 fois et affiche de 0 à 9

<https://codepen.io/anon/pen/pwdOr>

### Exercice 12

1. Créez une page HTML/CSS/JS
2. Dans le JS ajoutez un code qui affiche les chiffres de 1 à 50
3. Modifiez votre code de sorte qu’il affiche *fizz* pour les chiffres qui peuvent être divisés par 3 et *buzz* pour les multiples de 5
4. Apprenez cet exercice par cœur. C’est un test technique qu’on rencontre dans les processus de recrutement …

tip: if (i%3 == 0) et if (i%5 == 0)

## Gros TP de l’aprem: La suite de Fibonacci

Certes, à Pop on aime pas les trucs trop théoriques. Mais là, c’est un grand classique des tests de recruments, alors on y coupera pas …

Donc, codez-moi une putain de suite de Fibonacci: <https://fr.wikipedia.org/wiki/Suite_de_Fibonacci>

* Une suite de Fibonacci commence avec deux nombres: 0 et 1
* Chaque nouveau nombre est la somme des deux nombres précédents

Trop facile, non ?  
Si vous vous y prenez mal, vous pouvez bloquer votre machine et devoir faire un hard reboot… Ceci dit, si vous utilisez uniquement les notions qu’on vient de voir, vous ne bloquerez pas votre machine

1. Codez une suite de Fibonacci qui affiche 2500 nombres
2. Si votre machine n’a pas totalement planté, apprenez par cœur cet exercice.
3. Faites une page qui affiche autant d’images de petits lapins qu’il doit y en avoir dans une suite de Fibonacci, contrôlé par un input numérique 

tip: bloquez la valeur maximale de cet input. En effet, dès 20, on a plus de 6700 comme résultat …
