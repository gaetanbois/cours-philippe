# Cours de GIT, les bases

Philippe Pary 2017

## C’est quoi Git, à quoi ça sert ?

Git est un gestionnaire de versions.

* Sauvegarde toutes les versions de l’avancement votre projet
* Permet de gérer des publications (releases, via des tags)
* Permet de gérer des versions spécifiques (branches)

Et donc concrètement ?

* Ça sauvegarde votre code
* Ça vous permet de revenir en arrière si vous vous êtes trompé
* Ça vous facilite le travail à plusieurs sur un même projet

Convaincu ? Vous n’avez pas le choix de toutes façons : c’est utilisé partout !

## Créer un compte github !

Github est un réseau social pour développeurs. Ça vous permettra d’exposer vos savoir faire. De plus des recruteurs parcourrent github à la recherche de personnes.

On va donc aller y créer un compte …


## Créer son premier projet

Sur github, créez un premier projet. Appelez-le « first-HTMLCSS-project »

Suivez les directives indiquées. Dans un terminal, créez un nouveau dossier pour y mettre un projet vierge et saisissez les commandes suivantes:

    $ echo "first-HTMLCSS-projet" > README.md
    $ git init
    $ git add README.md
    $ git commit -m "first commit"
    $ git remote add origin https://github.com/<moncompte>/first-HTMLCSS-projet.git
    $ git push -u origin master

Allez sur github.com, voyez que les explications ont disparues pour laisser place à votre fichier README.md

## Ajouter des fichiers au projet

Prenez les fichiers d’un projet HTML/CSS et copiez les dans le dossier gité

    $ cp -a ~/code/projetHTMLCSS/* ~/code/exogit/

(il existe des tas d’autres manières de faire hein)

Une fois que les fichiers sont présents

    $ git add .
    $ git commit -m "Ajout des fichiers HTML et CSS"
    $ git push

Allez sur github.com, voyez les fichiers qui ont été ajoutés

## Effectuer des modifications

Faites une modification sur un fichier. Un truc rapide, sans se prendre le chou, genre modifier un `<h1>` en `<h2>` ou modifier une couleur dans un fichier CSS.

    $ git commit -am "Changement de style"
    $ git push

Allez sur github.com. Ouvrez le fichier en question, constatez la prise en compte des changements. Allez également regarder la liste des commits, il devrait y en avoir 3.

## Ajouter un fichier, supprimer un fichier

Ajoutons un nouveau fichier au projet

    $ touch nouveau-fichier.txt
    $ git add nouveau-fichier.txt
    $ git commit -m "Ajout d’un nouveau fichier"
    $ git push

Allez sur github.com, constatez l’apparition du fichier

Supprimons ce fichier inutile !

    $ git rm nouveau-fichier.txt
    $ git commit -m "Suppression du fichier"
    $ git push

Allez sur github.com, constatez la supression du fichier

## Wow, trop bien. On récapitule ?

Une fois la création initiale faite (en suivant les étapes indiquées par Github), Git est en 3 étapes.

1. On fait des modifications sur nos fichiers
2. On crée un `commit` (une sauvegarde git qui reste locale)
3. On publie notre ou nos `commit` via la commande `push` (envoie des commits vers le serveur distant, qui aura été configuré au début en suivant les instructions de github)

## Et donc

À partir de maintenant **tous** vos travaux, TP et exercices sont publiés sur github.com. Les formateurs se réservent le droit de ne pas vous aider si votre code n’y est pas publié (on est comme ça à Pop !)
