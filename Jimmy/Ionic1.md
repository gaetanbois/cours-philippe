# Ionic framework: création d’application *natives*

## Ionic ?

<http://ionicframework.com>

Ionic mixe Cordova, libraire de développement pour smartphones (iOS, Android …), NodeJS, serveur web permetant d’éxecuter du JavaScript côté serveur et Angular, patron (*framework*) de développement en JavaScript

Le but est de créer des applications mobiles en HTML, CSS et JavaScript, les trois technologies de base du web.  
Les applications créées sont disponible sous forme *native* (.apk pour Android par exemple) et peuvent être publiée sur les \*store

### Environnement de développement

Ionic propose une émulation sur votre PC pour le développement au quotidien (`$ ionic serve`)  

Ceci ne remplace pas l’usage réel de matériel. Soit via l’application **Ionic View** qui simplifie le boulot, soit en utilisant un périphérique relié au PC en mode debug (`$ ionic cordova run android`

### Natif, really ?

Si on génère des fichiers natifs installables, en réalité c’est la grosse arnaque …  
En effet on génère surtout un petit navigateur web optimisé pour notre application qui charge notre page tranquillou billou.

Par rapport à une application native réelle, les performances sont désastreuses. Ionic c’est bien pour des applications-mouchoirs. Pour un développement sérieux dans le temps, il va falloir apprendre Objective-C (iOS) ou Java-Android (… Android donc)

## Here it goes again

### Préparer l’environnement de travail

(vous êtes censés déjà avoir fait ça en amont, si ce n’est pas le cas, allez voir chez les grecs si y’a pas de la doc)

Installer nodejs et nodejs-legacy

    # apt install nodejs nodejs-legacy

Mettre à jour ses dépots npm

    # npm update

Mettre à jour npm (node package manager)

    # npm install -g npm

Installer Cordova et Ionic (sa mère)

    # npm install -g cordova ionic

Aller fumer une clope ou boire un café, c’est long …  
Vérifier que ça marche avec

    $ ionic start helloWorld tabs --type ionic1

Aller boire un café ou commencer le tabac. C’est long …  
Lancer le bousin

    $ cd helloWorld
    $ ionic cordova platform add android
    $ ionic serve

Sérieusement songer à commencer à picoler. C’est long  
Si ça marche, être heureux. Si ça ne marche pas, se mettre en PLS et appeler à l’aide un camarade ou Philippe sur Slack

* `NodeJS` est un serveur web qui permet d’éxecuter du JavaScript côté serveur. Il se base sur le moteur V8 qui est le moteur JavaScript de Chrome
* `npm` est node package manager. Il est à node ce que `apt` est à Debian.  
Là où Debian a une politique de qualité et liberté du code exigeante, `npm` fait n’importe quoi. Ça permet d’avoir plus de code, mais ça vous amènera à violer des licences, à importer des failles de sécurité et à rencontrer divers problèmes que justifieront votre salaire (qui vous servira à financer une thérapie nécessaire à surmonter les traumatismes générés par `npm`)

### Préparer son smartphone pour le debug

 Installer l’environnement Android
    Installer le SDK android : on le demande gentillement à son gentil formateur car ça prend plusieurs gigas à télécharger … 

    # cp -a android-sdk-linux /home/
    # chown -R popschool:popschool /home/android-sdk-linux
    # chmod -R 775 /home/android-sdk-linux

et on copie le dossier directement dans /home/ puis on ajoute ça à son ~/.bashrc  

    ANDROID_HOME=/home/android-sdk-linux
    export ANDROID_HOME
    PATH=$PATH:/home/android-sdk-linux:/home/android-sdk-linux/tools/:/home/android-sdk-linux/platform-tools
    export PATH

Installer les morceaux qui manquent du SDK Android en allant dans l’interface graphique

    $ android

Installer le logiciel `gradle`

    # apt install gradle

Installer un JDK

    # apt install openjdk-8-jdk

Configurer son téléphone en mode debug (7 pressions sur *Numéro de version* dans *À propos* dans *Paramètres*. Voir <https://developer.android.com/studio/run/device.html>) puis lancer la commande

    $ ionic cordova run android

C’est incroyble :')

*Nota: si vous rencontrez un soucis, tentez un `# apt install lib32stdc++6 lib32z1`*

* SDK: Software developpement kit. Ensemble de libraires contenant des fonctions complémentaires qui enrichissent le langage de programmation et de loigiciels complémentaires.
L’ensemble est fourni pour un type de développement donné (un drone, un système d’exploitation…). *attention !* à ne pas confondre avec un framework (un patron de programmation, qui impose la manière de travailler, comme rails)
* JDK: Java developpement kit. SDK propre à Java, tout connement. Ces gens sont snobs et ont remplacé le S par un J, juste pour frimer.
Android étant basé sur Java, il faut avoir un JDK en plus du SDK Android. Une cinquantaine de giga-octets pour créer des pacquets de quelques mega-octets :joy:
* Les lib32 sont souvent présentes sur le système. Elles servent à créer les APK en version 32bits (la plupart des périphériques Android ont des processeurs 32bits)


### Créer un .apk installable pour des tests

Le format .apk est à Android ce que .deb est à Debian.

On ne peut publier sur le PlayStore que des .apk appliquant des règles strictes (lire <http://ionicframework.com/docs/guide/publishing.html>)  
Mais on peut, pour raisons de test, créer un .apk que l’on pourra installer facilement.

    $ ionic cordova build android

Le fichier est dans `platforms/android/build/outputs/apk/android-debug.apk` (rien de moins, oui …)

* APK: Android PacKage. Système de paquet similaire à .deb. Comme les .deb, vous pouvez ouvrir un .apk par un simple clic-droit et en explorer le contenu. Vous y retrouverez votre code source HTML/CSS/JS tel quel !

## Utiliser Ionic view pour automatiser tout ça …

Configurer en mode debug puis brancher/débrancher tous les devices un à un, c’est lourd.  
Installer à la mano un .apk sur tous les devices c’est à peine mieux.

Ionic View résout tout !

Ionic view est une application à installer sur votre GSM/tablette/TV … et où, grace à vos identifiants, vous pourrez récupérer facilement les mises à jour de votre application.

Toutes les explications ici: <http://view.ionic.io/>

## Exercice

1. Installez l’environnement,
2. Créez une application de démonstration,
3. Lancez l’émulation en local,
4. Lancez l’émulation sur votre smartphone,
5. Compilez un .apk et installez le sur votre smartphone
6. Installez, configurez et utilisez Ionic view
7. Installez votre application via Ionic view

Ça sera déjà bien pour finir ce premier cours
