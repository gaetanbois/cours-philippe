# Cours de git, le poulailler

Philippe Pary, 2017

## C’est trop cool git …

On peut travailler à plusieurs, c’est un modèle en graphes mais appliqué à la vraie vie et tout et tout. 

Mais je ne sais que sauvegarder mon code … oups ?

## Dolly (alias le `clone`)

Échangez l’url d’un de vos projets github avec votre voisin. Récupérez l’adresse du dépôt via le bouton *clone or download*

    $ git clone <urlduprojetgithub>

Magie, un dossier du nom du projet github a été créé. Et dedans, il y a le code de votre camarade !

Faites des modifications simples mais visibles, comme changer la couleur de police. 

## Sting (alias `the police`)

Tentez un `commit` puis `push` … rhoooo vous êtes rejetés.

    $ git commit -am "G chenger lé coulleures. Cigner Kévin"
    $ git push

C’est normal: il y a des autorisations à avoir. Si le code est public, ce n’est pas pour autant que tout le monde peut le modifier.

Demandez à votre voisin de vous attribuer des droits de contributeur sur son projet git. Réessayez le push, ça va marcher

    $ git push

## Le poulailler

![Et je suis certain qu’il trouve ça drôle](images/poule.jpeg)

Retournez dans le dossier de votre projet à vous. Le votre, celui que votre voisin vient aimablement de pourir.

    $ git pull

Vous avez récupéré son `commit`. Vous pouvez le constater de deux manières:

    $ git log

(touch *q* pour sortir)

    $ git diff HEAD~1

(non, y’avait pas plus simple comme syntaxe)

## Il est mignon mon voisin, mais c’est un gros con

Tu es en colère, ton projet a été pourri par un boulet. On va corriger ça OKLM, tout va bien se passer.

1. Tu vires les droits de contributeur à ce boulet
2. Tu fais les commandes suivantes:

    $ git revert HEAD
    $ git push

3. Tu vas sur la page github de ton projet, et tu constates que tou va bien
4. Tu pars fumer une clope en te disant que plus jamais tu travailleras en collectif. Et t’aurais bien raison… si seulement c’était possible

## Et c’est que le début

On a les bases pour collaborer : 

* Créer un dépôt
* Envoyer du code
* Permettre à d’autres de contribuer
* Récupérer les changements
* Annuler des changements
* Avec un peu de (mal)chance, vous avez même vu comment résoudre un conflit

La prochaine fois on aborde les branches, et vous allez vivre en enfer à partir de ce jour }:>


