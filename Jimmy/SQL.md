# Cours de SQL, support de cours

Philippe Pary

## Introduire la notion de base de données

### TP

* Se connecter à PHPMyAdmin
* Créer une base `coursSQL1`
* Créer une table `eleves`
* Ajouter trois champs
    * `id` de type int, auto-incremental (A.I) et primaire
    * `firstname` de type varchar(50)
    * `lastname` de type varchar(50)

### Serveur

Stockage de données, accès multiple, DBA, SGDB (DBMS), BDD (DB)

### Base

### Table

### Colonne

Parler du typage, clef primaire, indexation, unique, auto-increment etc.

### Ligne

## Requêtes de base

### TP

Via l’interface de PHPMyAdmin

* Créer 5 élèves. Lire attentivement la requête affichée par PHPMyAdmin
* Modifier 2 élèves. Lire attentivement la requête affichée par PHPMyAdmin
* Supprimer un élève. Lire attentivement la requête affichée par PHPMyAdmin
* Rechercher un élève. Lire attentivement la requête affichée par PHPMyAdmin

### Notion de requête

Langage SQL, différences entre les technologies. Jeter les notions de CRUD, d’indexation, de jointure

### INSERT

### SELECT

### UPDATE

### DELETE

### TP

Via l’outil *Requête* de PHPMyAdmin 

* Créer 5 élèves.
* Modifier 2 élèves.
* Supprimer un élève.
* Rechercher un élève.

## Jointures

### TP

Créer une table `promotions`

* `id` de type int, auto-incremental (A.I) et primaire
* `name` de type varchar(255)
* `startdate` de type date
* `enddate` de type date

Insérer les nom des deux promotions

Modifier la table `eleves`

* Ajouter une colone `promotion_id`
* Affecter une promotion à chaque élève

### JOIN

## Exercice

### Niveau 1: Pratiquer CRUD

* Ajouter des élèves et des promotions, les modifier et les supprimer, notamment via des clauses *WHERE*
* Rajouter des colonnes aux tables: sexe, adresse (trois colonnes: adresse, codepostal et ville), date de naissance …
* Remplissez à fond la base de données et sauvegardez la (`Exporter`), vous pourrez ainsi rétablir la sauvegarde en cas de fausse manipulation
* Continuer à faire des requêtes pour vous spécialiser avec WHERE
    * Par début de code postal, par exemple sélectionner les 59
    * Dont la date de naissance est dans les années 80
    * Dont la promotion se déroule notamment en 2017

### Niveau 2: Étendre, travailler les jointures

Créer une table `subjects` pour stocker les matières et une table `validation` pour stocker quelles matières a validé chaque élèves

Remplir ces tables à la main

Savoir faire des requêtes (ex: quel élève de la promotion Aaron a validé HTML ?)

### Niveau 3: Foreign keys constraits

Ajoutez des contraintes de clefs aux tables et des `NOT NULL` aux colonnes où c’’est nécessaire. RTFM

Quelques requêtes avancées:

    * Nombre d’élèves ayant validé HTML par promotion
    * Moyenne d’âge par sexe, moyenne d’âge par promotion
    * Nombre de personnes ayant le même prénom, par prénom évidemment
