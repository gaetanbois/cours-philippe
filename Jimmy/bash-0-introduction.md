# Introduction à Bash

Philippe Pary. 12/16, PopSchool Lens  
http://pop.eu.com CC-BY-SA

## Présentation générale de Bash

* Bourne Again SHell (c’est très drôle)
* Projet GNU, 1988
* Shell par défaut des distribution Gnu/Linux et MacOS

Un shell est une interface entre l’utilisateur et le kernel du système d’exploitation (alias, OS, Operating system)

Une image à propos de ce que fait un OS:  
[![Image à propos des tâches d’un OS](https://drawings.jvns.ca/drawings/os-responsibilities.png "Tâches des OS")](https://drawings.jvns.ca/os-responsibilities/)

Une image à propos des SystemCalls, ce qu’un shell fera à votre place:  
[![Image sur comment parler à son OS](https://drawings.jvns.ca/drawings/syscalls.png "Comment parler à son OS")](https://drawings.jvns.ca/syscalls/)

### Fonctionnement

Bash peut fonctionner de deux manières:

* Interactif, en ligne de commande (celui que vous utiliserez le plus)
* Batch, via des scripts .sh (vous en crérez)

### Premier contact

1. Lancez un logiciel de terminal
2. Saisissez la commande suivante, sans le *$* (on y reviendra plus tard)  

    $ ls

3. Vous voyez le contenu de votre dossier personnel, saisissez les commandes suivantes  

    $ cd Documents
    $ ls

4. Vous voyez le contenu du dossier *Documents*. Changez encore de répertoire et listez-en le contenu
5. Quittez le terminal avec la commande Ctrl+D

## Parcourir l’arborescence

### Arborescence ?

Tous les systèmes informatiques, sauf Windows, fonctionnent comme un arbre.  
/ est la racine du système, tous les fichiers sont situés dans /, peu importe s’ils sont sur plusieurs disques différents

Chaque utilisateur a un dossier personnel. Dans les systèmes linux, ces dossiers sont dans /home, sur MacOS c’est dans /Users

Notez les notations suivantes:

* ~ : dossier personnel de l’utilisateur en cours
* . : le dossier courant
* .. : le dossier parent (/etc est le dossier parent de /etc/nginx)


### Premier contact

1. Lancez un logiciel de terminal
2. Saisissez la commande suivante, sans le *$* (on y reviendra plus tard)  

    $ cd /

3. Saisissez la commande suivante  

    $ ls

Vous voyez les dossiers et fichiers présents dans le dossier racine. Nous en reparlerons plus tard

### Commandes

* cd : Change Directory
* ls : List Segments. Liste les dossiers et les fichiers d’un dossier
* pwd : Print Working Directory. Affiche le nom du dossier courant

Mise en œuvre: 

    $ cd Documents
    $ ls
    $ pwd

## Copier/Coller/Effacer

* touch : créer un fichier (et bien plus encore)
* mkdir : créer un dossier
* cp : copier
* mv : move. Déplacer ou renommer
* rm : effacer un fichier
* rmdir : supprimer un dossier

Mise en œuvre : 

    $ mkdir tmp
    $ cd tmp
    $ touch file.txt
    $ mkdir myDir
    $ ls
    $ cp file.txt copy-of-file.txt
    $ ls
    $ mv file.txt myDir/file-moved.txt
    $ ls myDir
    $ ls
    $ rm myDir/file-moved.txt
    $ ls myDir
    $ rmdir myDir
    $ ls
    $ rm *
    $ ls
    $ pwd
    $ cd ..
    $ rm -R tmp

## lire du texte

* less: ouvrir un fichier pour le lire. Le logiciel est rapide et permet d’ouvrir des gros fichiers. Appuyez sur la touche *q* pour quitter
* head: affiche le haut du fichier
* tail: affiche le bas du fichier
* cat: affiche l’un à la suite de l’autre un ou plusieurs fichiers

Mise en œuvre: 

    $ cd /var/log
    $ head syslog
    $ tail syslog
    $ cat syslog* | less

## Éditer du texte

* nano : Ctrl+x pour quitter
* vi : voir la suite

Mise en œuvre: 

    $ cd
    $ nano .bashrc
    $ vi .bashrc

### Vi

Vi est un éditeur de texte VIsuel. Il fonctionne avec 3 modes: commande, insertion et visuel. On commence en mode commande.  
Il est présent sur tous les systèmes, savoir l’utiliser s’avère régulièrement indsipensable.

* i : entrer en mode insertion
* v : entrer en mode visuel
* esc : retourner un mode commande
* x : couper (plusieurs caractères en mode visuel)
* p : copier (en mode commande)
* :x : en mode commande, quitter et sauvegarder
* :q! : en mode commande, quitter sans sauvegarder

**Pro-tip** pour sorti à coup sur de Vi, faites esc puis :q!

## Obtenir de l’aide

Il y a de nombreuses manières d’obtenir de l’aide.

Un paramètre *-h* ou *--help* après la commande affiche souvent les options de la commande.

Les manpages contiennent le manuel du logiciel :  
    man ls

Vous quittez une manpage en appuyant sur la touche *q*

## Conclusion

On sait ce qu’est une arborescence, on sait se balader dedans. On sait faire des opérations primaires. Assez pour utiliser **git**

Reste à savoir faire des truc utiles.

## Exercices

1. Créez un dossier *« exo-bash »* et entrez dedans
2. Créez un fichier appelé *« liste\_noms.txt »*
3. Modifiez le fichier avec vi, insérez les noms des élèves de la promotion (fichier présent sur framagit)
4. Affichez les premiers noms de la liste, affichez les derniers noms
5. Copiez le fichier *« liste\_noms.txt »* et créez un fichier *« prenoms.txt »*
6. Modifiez le fichier avec nano, effacez les noms de famille
7. Affichez le contenu des deux fichiers avec une seul commande
8. Retournez dans votre dossier personnel en utilisant *~*
9. Déplacez le dossier *« exo-bash »* dans votre dossier *« Documents »*


