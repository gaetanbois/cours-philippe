# Cours de CSS pour le responsive Design

©Philippe Pary 2018

# Faire une galerie d’images responsive

1. Prenez 5 grandes images (dans dossier html3)
2. Créez en des miniatures

    $ for i in *.jpg ; do convert $i -resize 400x small-$i; done

3. Créez une page HTML affichant les images avec la balise `<picture>` avec la grande image par défaut, la petite pour les écrans inférieurs à 640px;
4. Dans le CSS, affichez les images comme des blocs (propriété `display`) et faites les se suivre via un `float: left`
5. Ajoutez un `max-width: 100%` afin d’éviter les débordements

# Faire un menu de haut de site

0. Créez un projet github, entre chaque étape, faites un `commit` et un `push`
1. Créez une page HTML
2. Ajoutez une section `<nav>`
3. Mettez-y 4 divs qui auront le contenu suivant:
* Accueil
* À propos
* Blog
* Contact
4. Mettez un `display: flex` sur la `<nav>`
5. Sur `<nav>`, ajoutez un `justify-content: space-around`. Mettez une couleur de fond, une couleur de texte. Faites changer la couleur de fond en cas de survol d’une `<div>` (sélecteur `nav > div`)
6. Sur les petits écrans (`@media (max-width: 640px)`) utilisez `flex-direction: column`

<https://codepen.io/anon/pen/yXXOZK?editors=1100>

# Faire un bas de page

Vous faites pareil, mais dans un `<footer>` avec dans 3 colones:

* vos coordonnées
* des liens vers vos réseaux sociaux
* des mentions légales

# Faire une grille

1. Récupérez les bonnes bouilles des joueurs du RC Lens (<http://rclens.fr/site/club_equipe/2017/index.php#ancreContenu>)
2. Créez une page HTML
3. Créez une grande `<section>` qui sera en `display: grid;`
4. Affichez l’entraîneur au milieu de la première ligne (ouais, faut retrouver sa photo …)
5. Affichez les gardiens sur la deuxième ligne
6. Affichez les défenseurs sur la troisième ligne
7. Affichez les milieux sur la quatrième ligne
8. Affichez les attaquants sur la dernière ligne (c’est la valeur `-1`)

<https://codepen.io/anon/pen/KoarBa>

# Box-sizing:

<https://developer.mozilla.org/fr/Apprendre/CSS/Introduction_%C3%A0_CSS/Le_mod%C3%A8le_de_bo%C3%AEte> 

<https://developer.mozilla.org/fr/docs/Web/CSS/box-sizing?v=control>

* Margin: espaces entre la bordure de l’élément et les autres éléments (extérieur)
* Padding: espaces entre la bordure de l’élément et son contenu (intérieur)
* Bordure: notez que la bordure peut avoir sa propre largeur …

Par défaut, la `width` calcule la largeur du contenu, sans les bordures, sans le padding, sans les margins (`content-box`)

On peut utiliser le `box-sizing: border-box` pour que la `width` calcule la largeur avec le padding, avec les bordures mais sans les margins.

# Notions de design

* Un logo
* Une typographie (une pour les titres, une pour le texte. Jamais plus de 3 sur une page)
* Une palette de couleurs (2 couleurs, outre le noir&blanc. Éventuellement 3)
* Zones à lire : colonnes de 80 lettres. Ne justifiez pas le texte. Typo de type `sans`
* Le visiteur parcours l’écran en Z : de en haut à gauche vers en bas à droite.
* Ne surchargez pas visuellement. Au moins il y en a, au plus votre site est agréable à regardez
* Ne surchargez pas d’effets de style. Un CSS compliqué, ça se ressent. Ça fait un site bordélique. Les jolis sites tiennent sur très peu de CSS …
* Pas d’auto-play de son. Jamais. Jamais.
* Grande image d’arrière-plan. Vidéo d’arrière plan, ça marche aussi !
* Observez les sites que vous visitez, tentez de reproduire quand quelque chose vous plaît avec la console de développement
