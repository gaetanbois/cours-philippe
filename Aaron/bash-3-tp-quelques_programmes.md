# Cours de bash, partie 4 : quelques programmes utiles

Philippe Pary, décembre 2016  
PopSchool Lens. CC-BY-SA

## Objectifs

Connaître des commandes utiles. Il s’agit essentiellement de manipulation de texte. Mais pas que.

Survolez le document, passez vite à la section TP qui vous sera bien plus utile. Inutile de tout savoir par cœur !

## Petits utilitaires

* ls: lister les segments (dossiers et fichiers)
* cd: change directory
* cp: copier
* df: espace disque
* du: espace utilisé par des segments (dossiers et fichiers)
* rm: supprimer un fichier
* pwd: afficher le répertoire de travail
* history: afficher l’historique des commandes

## Manipuler du texte

Le texte est issu du flux (retour renvoyé par d’autres commandes). Ces commandes sont souvent chaînées par des pipes (|)

Exemple pour voir si firefox tourne:

    $ ps aux | grep firefox

* cat: concaténer des fichiers
* cut: retirer du texte
* sort: trier
* uniq: supprimer des doublons
* grep: chercher des occurences
* sed: remplacer des occurences

## Autres

* echo: afficher du texte
* date: afficher la date
* top: moniteur système
* ps: processus en cours
* kill: envoyer un signal à un processus
* less: afficher un fichier à l’écran
* vi/nano: éditer des fichiers
* wget: télécharger depuis internet
* cron: gérer des tâches planifiées
* ln: créer des liens (raccourcis)
* tar: créer des fichiers compressés
* wc: compter les mots, les lignes etc.
* head/tail: afficher le haut/bas d’un fichier
* telnet: se connecter à un serveur
* find: rechercher des fichier, éxecuter des actions
* convert: modifier des images (colorimétrie, taille, orientation, filigrane etc.)

## Quelques raccourcis clavier

* touche tabulation: tout le temps, partout. Complète le nom de logiciel en cours, le nom de fichier, de dossier, fait le café et soigne les hémoroides.
* Ctrl+l: effacer l'écran (utile si vous venez de taper votre mot de passe en clair…)
* Ctrl+c: arrêt d'une commande
* Ctrl+z: suspendre (mettre en pause) une commande (*fg* pour reprendre)
* Ctrl+a: aller au début de ligne
* Ctrl+e: aller à la fin de ligne
* Ctrl+s: interruption de la sortie de terminal (masquer la saisie, lors d’un long défilement ou d’un *top* par exemple)
* Ctrl+q: annuler l'interruption de la sortie (reprenre la saisie)
* Ctrl+u: efface tout à gauche du curseur
* Ctrl+k: efface tout à droite du curseur
* Ctrl+d: efface le caractère courant, si la ligne est vide deconnexion

## Quelques concepts

### Redirections

1. le pipe **|**: sert à rediriger la sortie d’une commande vers une autre. Exemple:

    $ tail /var/log/syslog | grep CRON

2. **>**: redirige la sortie d’une commande vers un fichier, écrase le fichier existant. Exemple:

    $ find . -name *.php > liste_fichiers_php.txt

3. **>>**: comme avant, mais ajoute à la fin du fichier au lieu de l’écraser

    $ grep -i "failed" /var/log/auth.log >> failed_login_attempts.txt

4. **<**: redirige le contenu d’un fichier vers l’entrée d’une commande. On verra ça plus tard :)

### Boucles

Il est possible d’appliquer une même commande à plusieurs fichiers avec une boucle. Cette commande efface les fichiers qui se terminent par ~ (fichiers de sauvegarde automatique de gedit) :

    $ for i in *~; do rm -v $i ; done

On peut aussi imaginer la commande suivante:

    $ for fichier_css in *.css; do cp fichier_css ~/monnouveuaprojet/; done

Mais ça serait plus simple avec:

    $ cp *.css ~/monnouveuaprojet/

Notons que la commande find permet également d’appliquer des actions en masse via l’argument -exec:

    $ find . -name "*.css" -exec ls -l {} \;

{} est le fichier trouvé. \; marque la fin de la commande à éxecuter

### Variables

Il existe des variables système. Vous pouvez les afficher avec echo. Par exemple:

    $ echo $PATH

* HOME: répertoire utilisateur actuel
* USER: login utilisateur actuel
* PWD: répertoire courant
* SHELL: affiche de shell courant
* PATH: affiche les répertoires où se trouvent les logiciels qui peuvent être utilisés par l’utilisateur
* HOSTNAME: nom de la machine
etc.

On peut créer ses propres variables:

    $ bonjour="Bonjour !"
    $ echo $bonjour

### Expressions régulières

C’est un sujet qui mériterait un cours à lui seul. En gros c’est un dispositif carrément surpuissant qui permet de chercher des motifs dans le texte

Retenons deux trois choses:

En terminal:

* \* wildcard. de 0 à l’infini caractères
* ? de 1 à l’infini caractères

    $ find /etc -name *.conf

Dans une commande:

* \* wildcard. de 0 à l’infini du truc avant
* ? de 1 à l’infini du truc avant
* . un caractère (et donc .\* de 0 à l’infini caractères)
* ^ début de ligne
* $ fin de ligne
* \t tabulation
* \s espace

Exemples:

* Liste des élèves dont le prénom commence par C

    $ grep -e "\^C" liste-eleves.txt

* Liste des élèves dont le nom de famille commence par D

    $ grep -e ".*\sD" liste-eleves.txt

### Documentation

Quasiment tous les logiciels peuvent avoir de la documentation via *man*:

    $ man ls
    $ man grep

On trouve aussi *-h* ou *--help* pour avoir de l’aide:

    $ head --help
    $ wget -h

Mais surtout **internet regorge de documentation**. Utilisez donc un moteur de recherche !

Je vous invite à consulter (et améliorer) le wikibook suivant: <https://fr.wikibooks.org/wiki/Programmation_Bash>

### Customisation

Il existe un fichier .bashrc dans votre répertoire utilisateur. Il peut être utilisé pour de nomreuses customisations. Je vous laisse crawler le web trouver vos propres configurations :)

## Travaux pratiques

Ces travaux visent à vous apprendre à vous débrouiller. Toutes les commandes utilisées sont évoquées dans ce document (mais toutes les commandes évoquées ne sont pas utilisées)

### Première série: explorer rapidement du code

1. Aspirez le contenu du site <http://www.pasteque.org>
2. Listez les dossiers, entrez dans le dossier qui contient les fichiers de mise en page du site
3. Dans quel fichier se trouve la définition du style de *#about* ?
4. Dans quels fichiers trouve-t-on des définitions de *body* ?
5. Trouvez tous les fichiers dont le nom termine par *.html*
6. Listez les noms de fichiers dans lequel on trouve le nom *Philippe Pary*
7. Affichez la taille du dossier
8. Compressez le dossier
9. Affichez la taille du fichier compressé
10. Constatez que le texte se compresse très bien (plus de 50% !)

Bonus. Remplacez toutes les occurences de *pastèque* par *abricot*. C’est assez délicat, attention  
Bonus2. Dans le dossier theme/images/icons/ redimensionnez toutes les images .png en taille 64x64 tout en les renommant en .ico
(oui ça va les pixeliser à mort. C’est un exercice hein …)

### Seconde série: manipuler du texte

Récupérez le fichier liste-eleves.txt

1. N’affichez que les 5 premiers noms
2. N’affichez que les 3 derniers noms
3. N’affichez que les prénoms
4. N’affichez que les prénoms, sans les doublons
5. N’affichez que les noms de famille
6. N’affichez que les noms de famille, triez par ordre alphabétique
7. Affichez la taille du fichier
8. Compressez le fichier
9. Constatez encore une fois que ça se compresse vraiment bien, le texte
10. Créez un lien symbolique du fichier dans un autre répertoire (par exemple dans *~/Documents*)
11. Modifiez le nouveau fichier ainsi créé
12. Affichez l’ancien fichier, constatez que les changements ont été pris en compte

# C’est fini ici
