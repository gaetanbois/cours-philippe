# AJAX

Philippe Pary pour PopSchool, 2017. http://pop.eu.com

## Présentation rapide

*Bonjour les amis, je suis jQuery.get() et je suis votre nouvel ami*

    jQuery.ajax({
        type: 'GET',
        url: 'http://tp-lens.popschool.fr/api/users/',
        crossDomain: true,
        complete: function(rawResponse) {
            console.log("Raw response: ");
            console.log(rawResponse);
            users = rawResponse.responseJSON;
            console.log("Users: ");
            console.log(users);
            users.forEach(function(user) {
                console.log("User: " + user.id);
                console.log(user);
            });
        },
    });

*Ne me trouvez-vous pas magnifique ?*

## Un peu de bla-bla

XMLHTTPRequest, xhr de son petit nom, est un principe récent (2005). Il est à la base de ce qu’on a appelé alors le web2.0.

Il s’agit de récupérer à la volée des données sur un serveur back-end. Il est en effet plus rapide d’attraper une série de chiffres et de les placer dans des balises HTML (div, input, ul …) que de choper directement tout l’HTML.

Pour plusieurs raisons:
* 140 caractères (JSON) contre plus de 1400 caractèrs (HTML)
* xhr est *asynchrone*: le temps que les données charges, l’utilisateur peut continuer à utiliser le site web alors qu’avec une requête HTML classique, il doit attendre le chargement de la page
* Pensez à la vitesse d’un facebook par rapport à un forum classique … ce n’est pas qu’une question de serveur : très loin de là !

L’idée derrière tout ça: **séparer proprement back-end et front-end**

* PHP/Symfony (ou Ruby/rails) pour créer une API en back et la structure de certaines pages
* JS (jQuery/Angular/React…) pour l’**IHM** (interface homme-machine, alias *Human-computer interaction* HCI)

### Des métiers !

On a donc des *développeurs front-end*, des *développeurs back-end*. On cherche parfois des développeurs *full stack* (front-end **et** back-end). Pour gagner du temps, on parle de *front* et *back*

1. ⚠ le développeur front n’est pas un *designer* ni un *graphiste*, par contre il se doit d’au moins savoir intégrer des éléments (et de pouvoir agir come *intégrateur*, donc)
2. Un *intégrateur* est quelqu’un qui, partant d’une *maquette* fournie par un *graphiste* (ou un *designer*), va réaliser une page HTML+CSS (et éventullement avec un peu de JS pour les animations)
3. Un développeur back-end va souvent s’occuper de la base de données. Mais pour les bases de données très complexes, il existe une spécialisation: *DataBase Administrator* (alias *DBA*)
4. ⚠ il existe deux notions avec des noms proches de *front-end* et *back-end*, c’est le *front-office* et *back-office*. Le *front-office* désigne la partie utilisateur d’une application, le back-office son interface d’administration

### Hey, ho, let's go !

Allez, assez causé, on avance ! On va faire quelques exercices sur base du code de Daishi

Pour un peu plus se marer, on va utiliser un serveur central. L’URL est donc: http://tp-lens.popschool.fr/

## Exercice 1

Créer une application qui affiche proprement la liste des utilisateurs

## Exercice 2

Créer un application qui affiche la liste des noms des utilisateurs et, par clic sur un nom, affiche les détails dans un formulaire en dessous

NB: faites une requête vers api/users/2 (par exemple) pour avoir les détails

## Exercice 3

Faites en sorte que la liste des utilisateurs se rafraichisse *en loucedé* toutes les 5 secondes

## Exercice 4

Faire en sorte qu’en validant le formulaire, on modifie les informations de l’utilisateur

NB: faites attention à rafraîchir les données locales et éviter les *races conditions* !

## Exercice 5

Faites en sorte qu’on puisse effacer un utilisateur après 3 demandes de confirmation
