# Cours de bash, partie 5 : publier un site web

©Philippe Pary, 2017  
PopSchool. CC-BY-SA

## Objectifs

Savoir publier un site web

## Un serveur web: rapidement

Un serveur web est un ordinateur normal, mais il est tout le temps allumé et tout le temps connecté à Internet.

Il fait tourner des services. Certains accessibles par tout le monde (serveur web) d’autres accessibles uniquement par d’autres services ou logiciels présents sur le serveur (base de données)

Sur nos installations on retrouve:

* Apache (serveur web, simple *passe-plats* qui prend des fichiers et les envoie aux visiteurs)
* PHP (langage de script, logiciel lancé par apache quand le fichier à fournir se termine en .php)
* MySQL (base de données, qui sera utilisée par PHP pour générer les pages web)
* SSH (système d’accès à distance qui fournit notamment un accès SFTP)
* PHPMyAdmin (site web permettant d’administrer à distance les bases de données)

Un vrai serveur a une adresse IP propre sur Internet, sinon on parle de serveur local (ça sera le cas pour nous aujourd’hui)

Les serveurs locaux sont le plus souvent des serveurs de développement, qu’on oppose aux serveurs de production.

Le service web (ici Apache) peut gérer de multiples sites webs sur un même serveur. Ces sites webs se distinguent par leur nom de domaine (DN, de DNS). Apache propose un site web par défaut, qui se lance quoi qu’il se passe (nom de domaine inconnu, accès par adresse IP …)

Chaque site web dispose d’un fichier de configuration propre, qu’on trouvera dans le dossier `/etc/apache2/sites-available`. Si le site est présent dans `/etc/apache2/sites-enabled/` c’est qu’il est actif, sinon il est inactif. Chaque site web définit au moins:
* le nom de domaine (sinon c’est le site par défaut)
* le dossier racine, alias *root*, qui est le dossier où *apache* ira chercher les fichiers
* et le plus souvent l’emplacement des logs, journaux où sont enregistrées les erreurs et toutes les connexions (toutes, **toutes**)

## Publier (à l’arrache) un site statique

Nous venons de voir que par défaut, notre dossier racine (alias `root`) est `/var/www/html`. C’est donc dans ce dossier, sur le serveur web, que vous allez publier un site simple (ie HTML/CSS/JS, pas de PHP ni de ruby)

La plupart du temps vous aurez accès à votre serveur web via FTP (ce qui est très mal) ou en SFTP (ce qui est mieux)

Pour celà nous allons utiliser le logiciel `filezilla`. Pour l’installer, sur votre ordinateur faites

    # apt install filezilla

Une fois filezilla installé, lancez le logiciel. Paramétrez votre serveur (l’accès rapide ne marche pas pour le SFTP) et publiez plusieurs sites web simples et visitez les.

⚠ Attention à ne pas publier d’informations inutiles tels que les fichiers composer
⚠ Vous pouvez utiliser *Nautilus* (l’explorateur de fichiers). *Autres emplacements* -> *Connexion à un serveur* et vous indiquez `sftp://10.62.1.XX`. Il y aura moins d’options qu’avec *Filezilla*

## Publier (à l’arrache) un site dynamique

Nous allons publier un site PHP/MySQL. Sur votre serveur est déjà présent *PHPMyAdmin* via le dossier */phpmyadmin*.

1. Sur votre ordinateur, faites un *dump* (un export, une sauvegarde) de votre base de données
2. Sur le serveur, créez un compte utilisateur et une base de données
3. Sur le serveur, importez la sauvegarde de votre base de données
4. Publiez votre code PHP sur le serveur
5. Adaptez le login, le mot de passe et le nom de la base de données

⚠ encore une fois, attention à ne pas publier d’informations inutiles (fichiers composer, dossier `.git` etc.)

## Créer un nom de domaine de tests

### Résolution DNS locale

On peut créer des *résolutions* locales : des noms de domaines qui n’existeront que sur votre machine ou dont l’adresse IP correspondante sera valable uniquement sur votre ordinateur.

Ça se fait en modifiant le fichier `/etc/hosts` (valable également sur mac)

Vous créez des lignes ainsi

    10.62.1.72	philippe.popschool.local
    5.196.88.69	google.fr google.com

Vous pouvez vérifier que ça marche:

    $ ping philippe.popschool.local

### Configuration d’un virtual host

Dans le dossier `/etc/apache2/sites-available/` créez un fichier portant un nom du genre `philippe.popschool.local.conf`

⚠ le *.conf* final est important

    <VirtualHost *:80>
        ServerName philippe.popschool.local
        DocumentRoot /home/philippe/web/
    </VirtualHost>

Évidement, il vous faut créer le dossier *web*

On active le site web via la commande `a2ensite`

    # a2ensite philippe.popschool.local

⚠ lisez bien le retour de la commande `a2ensite` et faites sagement ce qu’elle vous dit

### Publier et voir le site web

Publiez le site web au nouvel emplacement si ce n’est pas déjà fait.

On peut maintenant aller sur son http://philippe.popschool.local et voir son site web publié

## L’intégration continue : publier moins à l’arrache, via git.

L’intégration continue vise à publier votre site web au fur et à mesure des modifications apportées sur une branche de votre dépôt git.
Soit sur la branche `master` soit sur la branche d’une version donnée (par exemple `7.0`)

On commence par créer son dossier web via un `git clone` classique. En HTTPS.

⚠ Un serveur web n’est pas un lieu pour coder: vous ne devez pas y faire de modifications sur votre code ni les pousser sur votre dépôt git.

### Manuellement

C’est le plus facile: vous créez votre dossier web via un `git clone` et par la suite, *a la mano*, vous faites des `git pull` quand des nouveautés sont disponibles

### Par tâche planifiée

Pas très compliqué non-plus: vous créez votre dossier web puis vous créez une tâche planfiée pour faire le `git pull` toutes les heures

### Par logiciel d’intégration continue

Il existe des logiciels d’intégration continue spécialisés qui vont lancer des *tests unitaires* et toute une série de vérifications sur votre serveur web une fois le `git pull` réalisé.

En effet, une mise à jour peut avoir des effets inattendus. Ça marche sur votre PC, pas sur le serveur. Il peut y avoir une différence de configuration, des logiciels absents etc.

Des logiciels comme *Jenkins* (<https://jenkins.io/>), s’ils sont compliqué à installer à votre niveau, vous épargnerons bien des soucis. Nous ne l’abordons pas dans le cadre des cours. Mais n’oubliez jamais qu’ils existent et apporteront une qualité professionnelle à vos logiciels.
